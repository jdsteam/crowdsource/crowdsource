# Crowdsource Survey App

## Run The Application

Create `.env` file

```sh
cp .env-sample .env
```

If you are working in local development, you can uncomment these lines from `docker-compose.yml` file to mount local filesystem to container's

```yaml
...
  vol_php_public:
    ...
    # driver: local
    # driver_opts:
    #   type: none
    #   device: $PWD/php/public
    #   o: bind
...
```

Then, build images and run containers

```sh
docker-compose up -d
```

Finally, you can access from your browser at http://localhost:8181

## Form Modification and Themes 

link tutorial modification form in Lime-Survey framework 

====> [Manual Themes Editor Modification](https://manual.limesurvey.org/Theme_editor) <====

for change themes, you can download free themes here 

====> [Themes download](https://www.limesurvey.org/limestore) <====

**how to upload new themes to application**

*  login as administrator 
*  click themes menu
*  click import inside menu themes
*  and then import themes in file .zip

![alt text](lib/img/menu-limesurvey.png)
![alt text](lib/img/inside-menu-themes.png)
![alt text](lib/img/upload-themes.png)

**how to change new themes survey application**

*  login as administrator 
*  click menu global setting 
*  click tab general in global menu setting 
*  choose themes in dropdown default themes 
*  then save configuration

![alt text](lib/img/menu-global-seting.png)
![alt text](lib/img/click-general.png)

## Reusable Survey or Question Using Export Import

**Rusable survey**

[Export survey]

*  Login as administrator 
*  Click menu survey on tab or header menu
*  Select the survey to export
*  Click button Display/export
*  then, Click button export

![alt text](lib/img/survey-menu.png)
![alt text](lib/img/list-survey.png)
![alt text](lib/img/overview-survey.png)
![alt text](lib/img/export-survey.png)

[Import survey]

*  Login as administrator 
*  Click menu survey on tab or header menu
*  Click button create survey
*  Click tab import survey 
*  Click choose file (extention file survey .lss)
*  Click button import file and save 

![alt text](lib/img/list-survey-import.png)
![alt text](lib/img/import-survey.png)

**Rusable question of survey**

[Export question]

*  Login as administrator
*  Click survey menu and select list survey 
*  Click menu list question and click button summary question on the right
*  then, Click button export on the top

![alt text](lib/img/list-survey-q.png)
![alt text](lib/img/list-question.png)
![alt text](lib/img/export-question.png)

[Import question]

*  Login as administrator
*  Click survey menu and select list survey 
*  Click menu list question
*  Click button import question on the top
*  Click choose file (extention file question .lsq)
*  then, Click button import

![alt text](lib/img/list-question-import.png)
![alt text](lib/img/import-file-question.png)