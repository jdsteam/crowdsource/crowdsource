## HOW TO EMBED FORM SURVEY

copy script bellow and edit url survey by generated survey when activated


```sh
<iframe 
    src="http://127.0.0.1:8181/index.php/136271?newtest=Y&lang=id" // <== URL SURVEY EXAMPLE
    style="border:0px #ffffff none;" 
    name="myiFrame" 
    scrolling="yes" 
    frameborder="0" 
    marginheight="0px" 
    marginwidth="0px" 
    height="400px" 
    width="600px" 
    allowfullscreen></iframe>
```

*Online Iframe generator check link below*

[Iframe Generator](https://www.iframe-generator.com/)

Sample Image output embed Survey to another site

![alt text](sc_embeed_survey.png)