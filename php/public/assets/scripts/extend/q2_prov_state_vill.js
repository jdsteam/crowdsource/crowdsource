var idDropdownProv ="";
var idDropdownKota ="";
var idDropdownKec ="";

function getIdFilterProv(qID1, gID1, sID1, filterqID1){
  idDropdownProv = "#answer"+sID1+"X"+gID1+"X"+qID1; 
}


function getIdFilterKota(qID2, gID2, sID2, filterqID2){
    idDropdownKota = "#answer"+sID2+"X"+gID2+"X"+qID2; 
}


function getIdFilterKecamatan(qID3, gID3, sID3, filterqID3){
    idDropdownKec = "#answer"+sID3+"X"+gID3+"X"+qID3; 
}

// get data API prov , Kota/kab , Kec/ Desa

var xhr = null;
function getProvinsi (){
    if(xhr != undefined && xhr != null){
        xhr.abort();
    }

    xhr = $.ajax({
        url:'http://bigdata.103.122.5.84.xip.io/master/provinsi?limit=10',
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(xxx){

        },
        complete:function(result){
            console.log("Complete :" + result);
        },
        success:function(data){
            var html ="";
            html += '<option value=>....</option>';            
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_provinsi_kode+'>'+val.kemendagri_provinsi_nama+'</option>';
            });
            $(idDropdownProv).html(html);
        },
        error:function(){
            // call back ketika api tidak ke get 
            getProvinsi();
        }
    })


}//end

var xhr2 = null;
function getKotaKab(idProv){
    if(xhr2 != undefined && xhr2 != null){
        xhr2.abort();
    }

    xhr2 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kabupaten?where={'kemendagri_provinsi_kode':'"+idProv+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
        },
        complete:function(result){
            console.log("Complete :" + result);
        },
        success:function(data){
            var html = "";
            html += '<option value=>.....</option>';
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_kabupaten_kode+'>'+val.kemendagri_kabupaten_nama+'</option>';
            });
            $(idDropdownKota).html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKotaKab(idProv);
        }
    })
}//end 

var xhr3 = null;
function getKecDesa(idKec){
    if(xhr3 != undefined && xhr3 != null){
        xhr3.abort();
    }

    xhr3 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kecamatan?where={'kemendagri_kabupaten_kode':'"+idKec+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
        },
        complete:function(result){
            console.log("Complete :" + result);
        },
        success:function(data){
            var html ="";
            html+= '<option value=>......</option>';
            $.each(data.data,function(i, val){
                html+= '<option value="'+val.kemendagri_kecamatan_kode+'">'+val.kemendagri_kecamatan_nama+'</option>';
            });
            $(idDropdownKec).html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKecDesa(idKec);
        }
    })
}//end 




$(document).ready(function(){

    console.log(idDropdownProv +" ID Provinsi");
    console.log(idDropdownKota +" ID Kota");
    console.log(idDropdownKec +" ID Desa/Kecamatan");
    console.log("vvvv"); 

    getProvinsi();
    var clear_dropdown = 0;
    $(idDropdownProv).change(function() {
        var idProv = $(this).val();
        $(idDropdownKota).val(clear_dropdown);
        $(idDropdownKec).val(clear_dropdown);
        getKotaKab(idProv);
    });

    $(idDropdownKota).change(function() {
        var idKec = $(this).val();
        $(idDropdownKec).val(clear_dropdown);
        getKecDesa(idKec);
    });

});