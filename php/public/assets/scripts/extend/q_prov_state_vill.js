var idDropdown ="";

function selectFilterByCode(qID, gID, sID, filterqID){
  console.log("Survey Kependudukan Daerah");
  
  console.log("Survey ID "+sID);
  console.log("Group ID "+gID);
  console.log("Question ID "+qID);

  idDropdown = "#answer"+sID+"X"+gID+"X"+qID+"Q_";
  
}


var xhr = null;
function getProvinsi (){
    if(xhr != undefined && xhr != null){
        xhr.abort();
    }

    xhr = $.ajax({
        url:'http://bigdata.103.122.5.84.xip.io/master/provinsi?limit=10',
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(xxx){

        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){
            var html ="";
            html += '<option value=>....</option>';            
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_provinsi_kode+'>'+val.kemendagri_provinsi_nama+'</option>';
            });
            $(idDropdown.toString()+"PROV").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get 
            getProvinsi();
        }
    })


}//end

var xhr2 = null;
function getKotaKab(idProv){
    if(xhr2 != undefined && xhr2 != null){
        xhr2.abort();
    }

    xhr2 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kabupaten?where={'kemendagri_provinsi_kode':'"+idProv+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){
            var html = "";
            html += '<option value=>.....</option>';
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_kabupaten_kode+'>'+val.kemendagri_kabupaten_nama+'</option>';
            });
            $(idDropdown.toString()+"CITY").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKotaKab(idProv);
        }
    })
}//end 

var xhr3 = null;
function getKecDesa(idKec){
    if(xhr3 != undefined && xhr3 != null){
        xhr3.abort();
    }

    xhr3 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kecamatan?where={'kemendagri_kabupaten_kode':'"+idKec+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){
            var html ="";
            html+= '<option value=>......</option>';
            $.each(data.data,function(i, val){
                html+= '<option value="'+val.kemendagri_kecamatan_kode+'">'+val.kemendagri_kecamatan_nama+'</option>';
            });
            $(idDropdown.toString()+"VILL").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKecDesa(idKec);
        }
    })
}//end 



$(document).ready(function(){
 
    // console.log(idDropdown+"_PROV");
    console.log("oowowoow"); 
    
    for (let i = 1; i <= 10; i++) {   
        $(idDropdown.toString()+"PROV option[value='"+i+"']").remove();
        $(idDropdown.toString()+"CITY option[value='"+i+"']").remove();
        $(idDropdown.toString()+"VILL option[value='"+i+"']").remove();
    }
    
    getProvinsi();
    var clear_dropdown = 0;
    $(idDropdown.toString()+"PROV").change(function() {
        var idProv = $(this).val();
        $(idDropdown.toString()+"CITY").val(clear_dropdown);
        $(idDropdown.toString()+"VILL").val(clear_dropdown);
        getKotaKab(idProv);
    });

    $(idDropdown.toString()+"CITY").change(function() {
        var idKec = $(this).val();
        $(idDropdown.toString()+"VILL").val(clear_dropdown);
        getKecDesa(idKec);
    });
});