var idDropdown2 ="";

function selectFilterByCodeJabar(qID, gID, sID, filterqID){
  console.log("Survey Kependudukan Daerah 2");
  
  console.log("Survey ID "+sID);
  console.log("Group ID "+gID);
  console.log("Question ID "+qID);

  idDropdown2 = "#answer"+sID+"X"+gID+"X"+qID+"Q2_";
  
}

var xhr2 = null;
function getKotaKab2(){
    if(xhr2 != undefined && xhr2 != null){
        xhr2.abort();
    }

    xhr2 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kabupaten?where={'kemendagri_provinsi_kode':'32'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){       
            var html = "";
            html += '<option value=>.....</option>';
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_kabupaten_kode+'>'+val.kemendagri_kabupaten_nama+'</option>';
            });
            $(idDropdown2.toString()+"CITY2").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKotaKab2();
        }
    })
}//end 

var xhr3 = null;
function getKecamatan2(idCITY2){
    if(xhr3 != undefined && xhr3 != null){
        xhr3.abort();
    }

    xhr3 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kecamatan?where={'kemendagri_kabupaten_kode':'"+idCITY2+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){
            var html ="";
            html+= '<option value=>......</option>';
            $.each(data.data,function(i, val){
                html+= '<option value="'+val.kemendagri_kecamatan_kode+'">'+val.kemendagri_kecamatan_nama+'</option>';
            });
            $(idDropdown2.toString()+"DIST2").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKecamatan2(idCITY2);
        }
    })
}//end 

var xhr4 = null;
function getDesa2(idDesa){
    if(xhr4 != undefined && xhr4 != null){
        xhr4.abort();
    }

    xhr4 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/desa?where={'kemendagri_kecamatan_kode':'"+idDesa+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){    
            var html ="";
            html+= '<option value=>......</option>';
            $.each(data.data,function(i, val){
                html+= '<option value="'+val.kemendagri_desa_kode+'">'+val.kemendagri_desa_nama+'</option>';
            });
            $(idDropdown2.toString()+"VILL2").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getDesa2(idDesa);
        }
    })
}



$(document).ready(function(){   
    for (let i = 1; i <= 10; i++) {   
        $(idDropdown2.toString()+"CITY2 option[value='"+i+"']").remove();
        $(idDropdown2.toString()+"DIST2 option[value='"+i+"']").remove();
        $(idDropdown2.toString()+"VILL2 option[value='"+i+"']").remove();
    }
    
    getKotaKab2();

    var clear_dropdown = 0;
    $(idDropdown2.toString()+"CITY2").change(function() {
        var idProv2 = $(this).val();
        $(idDropdown2.toString()+"DIST2").val(clear_dropdown);
        $(idDropdown2.toString()+"VILL2").val(clear_dropdown);
        getKecamatan2(idProv2);
    });

    $(idDropdown2.toString()+"DIST2").change(function() {
        var idCITY2 = $(this).val();
        $(idDropdown2.toString()+"VILL2").val(clear_dropdown);
        getDesa2(idCITY2);
    });


});