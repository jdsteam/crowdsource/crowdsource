var idDropdown3 ="";

function selectFilterByCodeJabarKotKec(qID, gID, sID, filterqID){
  console.log("Survey Kependudukan Daerah 3");
  
  console.log("Survey ID "+sID);
  console.log("Group ID "+gID);
  console.log("Question ID "+qID);

  idDropdown3 = "#answer"+sID+"X"+gID+"X"+qID+"Q3_";
  
}

var xhr2 = null;
function getKotaKab3(){
    if(xhr2 != undefined && xhr2 != null){
        xhr2.abort();
    }

    xhr2 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kabupaten?where={'kemendagri_provinsi_kode':'32'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){       
            var html = "";
            html += '<option value=>.....</option>';
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_kabupaten_kode+'>'+val.kemendagri_kabupaten_nama+'</option>';
            });
            $(idDropdown3.toString()+"CITY3").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKotaKab3();
        }
    })
}//end 

var xhr3 = null;
function getKecamatan3(idCITY3){
    if(xhr3 != undefined && xhr3 != null){
        xhr3.abort();
    }

    xhr3 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kecamatan?where={'kemendagri_kabupaten_kode':'"+idCITY3+"'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){
            var html ="";
            html+= '<option value=>......</option>';
            $.each(data.data,function(i, val){
                html+= '<option value="'+val.kemendagri_kecamatan_kode+'">'+val.kemendagri_kecamatan_nama+'</option>';
            });
            $(idDropdown3.toString()+"DIST3").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKecamatan3(idCITY3);
        }
    })
}//end 




$(document).ready(function(){     
    for (let i = 1; i <= 10; i++) {   
        $(idDropdown3.toString()+"CITY3 option[value='"+i+"']").remove();
        $(idDropdown3.toString()+"DIST3 option[value='"+i+"']").remove();
    }
    
    getKotaKab3();

    var clear_dropdown = 0;
    $(idDropdown3.toString()+"CITY3").change(function() {
        var idProv3 = $(this).val();
        $(idDropdown3.toString()+"DIST3").val(clear_dropdown);
        getKecamatan3(idProv3);
    });


});