var idDropdown4 ="";

function selectFilterByCodeJabarKota(qID, gID, sID, filterqID){
  console.log("Survey Kependudukan Daerah 4");
  
  console.log("Survey ID "+sID);
  console.log("Group ID "+gID);
  console.log("Question ID "+qID);

  idDropdown4 = "#answer"+sID+"X"+gID+"X"+qID+"Q4_";
  
}

var xhr2 = null;
function getKotaKab4(){
    if(xhr2 != undefined && xhr2 != null){
        xhr2.abort();
    }

    xhr2 = $.ajax({
        url:"http://bigdata.103.122.5.84.xip.io/master/kabupaten?where={'kemendagri_provinsi_kode':'32'}",
        method:'GET',
        cache: false,
        dataType:'JSON',
        header:{"Access-Control-Allow-Origin": "*"},
        // data:formData,
        beforeSend:function(data){
            // console.log("before send :"+ data);
        },
        complete:function(result){
            // console.log("Complete :" + result);
        },
        success:function(data){       
            var html = "";
            html += '<option value=>.....</option>';
            $.each(data.data,function(i, val){
                html += '<option value='+val.kemendagri_kabupaten_kode+'>'+val.kemendagri_kabupaten_nama+'</option>';
            });
            $(idDropdown4.toString()+"CITY4").html(html);
        },
        error:function(){
            // call back ketika api tidak ke get
            getKotaKab4();
        }
    })
}//end 


$(document).ready(function(){   
    console.log(idDropdown4);
    
    for (let i = 1; i <= 10; i++) {   
        $(idDropdown4.toString()+"CITY4 option[value='"+i+"']").remove();
    }
    
    getKotaKab4();

});