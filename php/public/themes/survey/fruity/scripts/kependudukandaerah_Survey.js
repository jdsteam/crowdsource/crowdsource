var idDropdown ="";

function selectFilterByCode(qID, gID, sID, filterqID){
  console.log("Survey Kependudukan Daerah");
  
  console.log("Survey ID "+sID);
  console.log("Group ID "+gID);
  console.log("Question ID "+qID);

  idDropdown = "#answer"+sID+"X"+gID+"X"+qID+"Q_";
  
}

$(document).ready(function(){
    console.log(idDropdown+"_PROV");
    console.log("berubah 7777777777777");
    
    for (let i = 1; i <= 10; i++) {
       
        $(idDropdown.toString()+"PROV option[value='"+i+"']").remove();
        $(idDropdown.toString()+"CITY option[value='"+i+"']").remove();
        $(idDropdown.toString()+"VILL option[value='"+i+"']").remove();
    }
    
    var Provinsi = {
        A:'JAWA BARAT', 
        B:'JAWA TENGAH', 
        C:'JAWA TIMUR'
    }; 
    
    var Kota = {
        A1:'BANDUNG',
        A2:'TASIK',
        A3:'SUMEDANG',
        A4:'MAJALENGKA',
        B1:'CIREBON',
        B2:'SEMARANG',
        B3:'JOGJAKARTA',
        B4:'BEREBES',
        C1:'SURABAYA',
        C2:'MALANG',
        C3:'KUDUS',
        C4:'JEPARA',
    }

    var Desa = {
        A11:'CITARUM',
        A12:'ANTAPANI',
        A13:'KIARA CONDONG',
        A21:'CIKURUBUK',
        A22:'SELAAWI',
        A23:'PASEH',
        A31:'CIMENYAN',
        A32:'CIMALAKA',
        A33:'JATINANGOR',
        A41:'JATIWANGI',   
        A42:'KADIPATEN',   
        A43:'KAMPUNG SAWAH', 
        B11:'KP CIREBON 1',  
        B12:'KP CIREBON 2',  
        B13:'KP CIREBON 3',
        B21:'KP SEMARANG 1', 
        B22:'KP SEMARANG 2', 
        B23:'KP SEMARANG 3',
        B31:'KP JOGJAKARTA 1',
        B32:'KP JOGJAKARTA 2',
        B33:'KP JOGJAKARTA 3',
        B41:'KP BEREBES 1',
        B42:'KP BEREBES 2',
        B43:'KP BEREBES 3',
        C11:'KP SURABAYA 1',
        C12:'KP SURABAYA 2',
        C13:'KP SURABAYA 3',
        C21:'KP MALANG 1',
        C22:'KP MALANG 2',
        C23:'KP MALANG 3',
        C31:'KP KUDUS 1',
        C32:'KP KUDUS 2',
        C33:'KP KUDUS 3',
        C41:'KP JEPARA 1',
        C42:'KP JEPARA 2',
        C43:'KP JEPARA 3',
    }


    

    $.each(Provinsi,function(i, val){
         $(idDropdown.toString()+"PROV").append(`<option value="${[i]}">${val}</option>`);
    });

    $.each(Kota,function(i, val){
            $(idDropdown.toString()+"CITY").append(`<option value="${[i]}">${val}</option>`);
    });

    $.each(Desa,function(i, val){
        $(idDropdown.toString()+"VILL").append(`<option value="${[i]}">${val}</option>`);
    });

 
   $(idDropdown.toString()+"CITY").children('option:gt(0)').hide();
   $(idDropdown.toString()+"PROV").change(function() {
       $(idDropdown.toString()+"CITY").children('option').hide();
       $(idDropdown.toString()+"CITY").children("option[value^=" + $(this).val() + "]").show()
   })

   $(idDropdown.toString()+"VILL").children('option:gt(0)').hide();
   $(idDropdown.toString()+"CITY").change(function() {
       $(idDropdown.toString()+"VILL").children('option').hide();
       $(idDropdown.toString()+"VILL").children("option[value^=" + $(this).val() + "]").show()
   })

    $(idDropdown.toString()+"PROV").change(function(){
        $(idDropdown.toString()+"CITY").val(0);
        $(idDropdown.toString()+"VILL").val(0);
    });
    $(idDropdown.toString()+"CITY").change(function(){
        $(idDropdown.toString()+"VILL").val(0);
    });

});